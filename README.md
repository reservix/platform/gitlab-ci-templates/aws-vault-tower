# aws-vault-tower

The AWS Vault Tower automates authentication with common services provided by the Platform Engineering Team and/or AWS.

## Documentation
Please also take our [documentation in Confluence](https://reservix.atlassian.net/l/cp/5KjqPHAQ) into account.

## Requirements for your jobs/images

| Requirement        | Usage                                               | additional information                                    |
|--------------------|-----------------------------------------------------|-----------------------------------------------------------|
| jq, curl           | mandatory                                           | Autoinstall in default images and all alpine based images |
| aws-cli            | only in deprecated `CONFIGURE_ECR` feature          | Autoinstall in default images and all alpine based images |

## deprecations

| deprecated version | removed in version | Feature                 | deprecation information                                             |
|--------------------|--------------------|-------------------------|---------------------------------------------------------------------|
| v5.1               | unknown            | `CONFIGURE_ECR: "True"` | Ask the platform team if removing the option breaks your pipelines. |

## Upgrade from v5.x

If you use `!reference` notation you need to add `id_tokens` to your jobs. Compare [Configuration](#configuration) and [Usage](#usage)

⚠️ Versions earlier then 5.2.0 are broken since Gitlab v17 ⚠️ 

### Technical background for id_tokens breaking change

For our login process we had to migrate to `id_tokens` were we ealier depended on `CI_JWT_TOKEN`.

`CI_JWT_TOKEN` was removed in Gitlab v17. 

## Upgrade from earlier versions

Information about older versions and how to upgrade from them can be found [here](change_archive.md)

## Configuration

The following input variables can be used and have the following defaults:
```
FIX_UMASK_BUG: "true"
CONFIGURE_GIT: "true"
CONFIGURE_SSH: "true"
CONFIGURE_VAULT: "true"
CONFIGURE_AWS: "true"
CONFIGURE_ANSIBLE: "false"
CONFIGURE_ECR: "false"  # deprecated
SSH_USER_NAME: ""
SSH_USER_EMAIL: ""
JUMPHOST_HOST: ""
JUMPHOST_HOSTNAME: ""
VAULT_ADDR: ""
AWS_VAULT_ROLE: ""
VAULT_ROLE_NAME: ""
AWS_VAULT_TOWER_DEBUG: "false"
```

## Usage

```yaml
variables:
  AWS_VAULT_ROLE: "TerraformDevops"
 
include:
  - project: devops/ci-templates/aws-vault-tower
    ref: v5.2.1
    file: .aws_vault_tower.yml
 
 
use_secrets:
  extends: .aws_vault_tower
  stage: build
  script:
    - apk add vault jq
    - vault kv get foo

use_secrets_advanced:
  extends: .aws_vault_tower
  image: alpine
  stage: build
  environment:
    name: ci
    deployment_tier: development
  before_script:
    - apk add vault jq
    - !reference [.aws_vault_tower, before_script]
  script:
    - vault kv get foo

custom_secrets:
  # no extends here
  image: alpine
  stage: build
  id_tokens: !reference [.aws_vault_tower, id_tokens]
  before_script:
    - !reference [.aws_vault_tower, before_script]
  script:
    - vault kv get foo

```

### ⚠️ Important to know about before_script and id_tokens with aws-vault-tower

AWS Vault Tower defines a `before_script` and `id_tokens` for your job. If you use either of them in your job, you will overwrite them.

If you need one of them, you can use `!reference` notation as shown in [Usage](#usage). It is adviced to use `extends: .aws_vault_tower` either way, but can be used without it. 

## How to use COFNIGURE_ECR (deprecated)

⚠️ `CONFIGURE_ECR: "True"` is deprecated. If you cannot push your images without it, ask the platform team to fix your iam permissions. ⚠️

1. configure aws-vault-tower in your job
1. set `CONFIGURE_ECR: "True"`
1. login to ECR using aws-cli:
   ```sh
   eval $(aws ecr --profile=ecr get-login --no-include-email)
   ```

## Hints for Platform Team Developers

⚠️ For this Repository, mirroring is setup between the old inhouse gitlab and gitlab.com. Please update this repository on our old gitlab instance only  ⚠️