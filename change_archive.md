# Information about unsupported versions

## deprecations

| deprecated version | removed in version | Feature                 | deprecation information                                             |
|--------------------|--------------------|-------------------------|---------------------------------------------------------------------|
| v5.1               | unknown            | `CONFIGURE_ECR: "True"` | Ask the platform team if removing the option breaks your pipelines. |

## Upgrade from 4.x to 5.x

* If you use Ansible in your Pipeline you need to set `CONFIGURE_ANSIBLE: "true"` 
* Vault Client is not installed anymore, hence if you need to interact with Vault from your script either provide it within your Dockercontainer or use the REST API of Vault

## Upgrade from 3.x to 4.x

It is no longer possible to configure aws without vault, as we now use temporary credentials
that are provided by vault. 

The variable `CD_AWS_CONFIG` is removed and replaced by `AWS_VAULT_ROLE`. The roles have a slightly 
different pattern

| (old) `CD_AWS_CONFIG` | (new) `AWS_VAULT_ROLE` |
|-----------------------|------------------------|
| terraform.management  | TerraformManagement    |
